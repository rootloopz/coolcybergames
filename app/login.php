<?php
// Get the user input from the HTML form
$username = $_POST['username'];
$password = $_POST['password'];

// Connect to the database
$servername = "localhost";
$dbname = "your_database";
$db_username = "your_username";
$db_password = "your_password";

$conn = new mysqli($servername, $db_username, $db_password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Construct the SQL query (vulnerable to SQL injection)
$sql = "SELECT * FROM users WHERE username = '" . $username . "' AND password = '" . $password . "'";

// Execute the query
$result = $conn->query($sql);

// Check if any rows were returned
if ($result->num_rows > 0) {
    // User is authenticated
    echo "Login successful!";
} else {
    // User login failed
    echo "Invalid credentials.";
}

// Close the database connection
$conn->close();
?>

