# Use an official PHP runtime as the base image
FROM php:7.4-apache

# Install necessary packages
RUN apt-get update && apt-get install -y \
    libpng-dev \
    libjpeg-dev \
    libmcrypt-dev \
    mariadb-client \
    && rm -rf /var/lib/apt/lists/*

# Enable necessary Apache modules
RUN a2enmod rewrite

# Install PHP extensions
RUN docker-php-ext-install mysqli pdo_mysql gd

# Copy your application code to the container
COPY ./app /var/www/html

# Set the document root directory
WORKDIR /var/www/html

# Expose port 80 for Apache
EXPOSE 80

# Start the Apache server
CMD ["apache2-foreground"]

